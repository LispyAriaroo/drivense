
exports.up = function(knex, Promise) {
    return knex.schema.createTable('admins', function(table) {
        table.increments('id').primary();
        table.string('admin_uuid');

        table.string('first_name');
        table.string('last_name');
        table.string('email_address');
        table.string('password_hash');

        table.boolean('is_reviewer');
        table.boolean('is_processor');

        table.timestamps();

        table.index(['admin_uuid', 'email_address']);
    })
};

exports.down = function(knex, Promise) {

};
