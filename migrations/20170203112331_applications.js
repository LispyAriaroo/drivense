
exports.up = function(knex, Promise) {
    return knex.schema.createTable('applications', function(table) {
        table.increments('id').primary();
        table.string('application_uuid');

        table.integer('applicant_user_id');

        table.enum('application_type', ['Articulated Vehicle', 'Commercial', 'Private', 'Motorcycle']).notNull();

        table.float('driver_test_score');

        table.string('state_made_from');

        table.string('geodata_latitude');
        table.string('geodata_longitude');

        table.enum('first_time_or_renewal', ['First time', 'Renewal']).notNull();

        table.boolean('is_reviewer_pending');
        table.boolean('is_processor_pending');

        table.boolean('did_reviewer_reject');
        table.boolean('did_reviewer_approve');
        table.string('reviewer_comment');
        table.date('date_reviewed');

        table.boolean('did_processor_reject');
        table.boolean('did_processor_approve');
        table.string('processor_comment');
        table.date('date_processed');

        table.index(['is_reviewer_pending', 'is_processor_pending',
                    'did_reviewer_reject', 'did_reviewer_approve',
                    'did_processor_reject', 'did_processor_approve']);
    })
};

exports.down = function(knex, Promise) {

};
