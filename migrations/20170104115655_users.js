
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('users', function(table) {
            table.increments('id').primary();
            table.string('user_uuid');

            table.string('first_name');
            table.string('last_name');
            table.string('email_address');
            table.string('phone_number');

            table.date('date_of_birth');
            table.string('gender');
            table.string('state_of_origin');

            table.string('occupation');
            table.string('residential_address');

            table.string('image_url');

            table.string('password_hash');

            table.timestamps();

            table.index(['user_uuid', 'email_address', 'phone_number']);
        })
    ])
};

exports.down = function(knex, Promise) {

};
