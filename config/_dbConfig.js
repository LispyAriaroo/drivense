
var knex = require('knex');
var KnexFile = require('../knexfile');

var knexDbConfigs = {};
knexDbConfigs['development'] = KnexFile['development'];
knexDbConfigs['production'] = KnexFile['production'];

var dbEnvVar = 'Sbm_DbEnv';

if (process.env[dbEnvVar] !== undefined) {
    var dbEnv = process.env[dbEnvVar];
    console.log("Sbm_DbEnv: " + dbEnv);

    var currentDbConfig = knexDbConfigs[dbEnv];
    var sbmDb = knex(currentDbConfig);

    module.exports = sbmDb;
} else {
    throw new Error('Environment variable: ' + dbEnvVar + ' MUST exist!');
}
