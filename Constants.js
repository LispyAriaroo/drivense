
var constants = {};

constants.AppName = "Sport Bet Manager";

// Nginx directs traffic on port 80 to port 3000
// On ubuntu VPS use command:
// sudo emacs /etc/nginx/sites-enabled/default
// to view configuration
constants.SERVER_PORT = 3100;
constants.API_VERSION_KEY = "version";
constants.API_VERSION_VALUE = "/api/v1";

constants.SBM_DOMAIN = "http://ec2-54-146-182-97.compute-1.amazonaws.com";

module.exports = constants;
