'use strict';

var jwt = require('jsonwebtoken');


function TokenVerifier() {}

var tokenVerifier = new TokenVerifier();


tokenVerifier.verifyToken = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        var jwtConfigSecret = process.env.SECRET;

        jwt.verify(token, jwtConfigSecret, function(err, decoded) {
            if (err) {
                return res.status(403).json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        console.log("No token was provided");
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};


module.exports = tokenVerifier;
