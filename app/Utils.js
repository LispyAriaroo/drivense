'use strict';


var request = require('request');
//var GoogleLibPhone = require('google-libphonenumber');
var AwesomePhoneNumber = require('awesome-phonenumber');

var Rest = require('./enums/Rest');
//var SmsTorrentConfig = require('../config/smstorrent');
var SendGridConfig = require('../config/sendgrid');
var Constants = require('../Constants');

var utils = {};


utils.spitOutFailure = function (errMsg, res) {
    var response = {};
    response[Rest.SUCCESS] = false;
    response[Rest.MESSAGE] = errMsg;
    res.json(response);
};


utils.doGet = function (hostParam, pathParam, callback) {
    request(hostParam + pathParam, function (error, response, body) {
        //console.log("Error: " + error + ", response: " + JSON.stringify(response));

        if (!error && response.statusCode == 200) {
            callback(body);
        } else {
            console.log('[Utils.js] ERROR or statusCode NOT 200. \n'
                + 'Error: ' + JSON.stringify(error) + ", response: " + JSON.stringify(response));
        }
    });
}

// http://stackoverflow.com/questions/10645994/node-js-how-to-format-a-date-string-in-utc
utils.dateFormat = function (date, fstr, utc) {
    utc = utc ? 'getUTC' : 'get';

    return fstr.replace (/%[YmdHMS]/g, function (m) {
        switch (m) {
            case '%Y': return date[utc + 'FullYear'] (); // no leading zeros required
            case '%m': m = 1 + date[utc + 'Month'] (); break;
            case '%d': m = date[utc + 'Date'] (); break;
            case '%H': m = date[utc + 'Hours'] (); break;
            case '%M': m = date[utc + 'Minutes'] (); break;
            case '%S': m = date[utc + 'Seconds'] (); break;

            default: return m.slice (1); // unknown code, remove %
        }
        // add leading zero if required
        return ('0' + m).slice (-2);
    });
}


var emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(-)*(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

utils.isEmailOk = function(inputVal) {
    return inputVal && inputVal.length > 0 && inputVal.match(emailPattern);
};

utils.isNumeric = function(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};

// https://github.com/grantila/awesome-phonenumber
utils.isNigerianPhoneNumberOk = function(thePhoneNum) {
    var isOk = false;

    var pn = new AwesomePhoneNumber(thePhoneNum, 'NG');
    if(pn.isValid()) {
        return pn.getNumber();
    } else {
        return false;
    }
};

/* http://stackoverflow.com/questions/2175512/javascript-expression-to-generate-a-5-digit-number-in-every-case */
/*
    Will be a 4 digit code
*/
utils.generateUserTransactionPassCode = function() {
    return "" + (Math.floor(Math.random() * 9000) + 1000);
}

utils.generateWorkerLoginCode = function() {
    return "" + (Math.floor(Math.random() * 9000) + 1000);
}

// https://github.com/AndrewKeig/infobip-sms
utils.sendSms = function(recipientPhone, msgContent) {
    var Infobip = require('infobip-sms');
    var sms = new Infobip('Efeariaroo', 'Test12345');

    var normalizedRecipientPhone = new AwesomePhoneNumber(recipientPhone, 'NG');
    if(normalizedRecipientPhone.isValid()) {
        var sender = 'MyChange(www.mychange.com)';
        var recipients = [{ gsm: normalizedRecipientPhone.getNumber(), messageId : 'validity' }];
        var msg = msgContent.toString('utf8');
        var options = { text : true };

        // sms.send(sender, msg, recipients, options, function(err, r){
        //     if (err) {
        //         console.log(err);
        //     } else {
        //         console.log("Seems SMS was sent successfully!");
        //     }
        // });
    }
};

utils.sendSupervisorAddedToLocationEmail = function(supervisorFirstName, supervisorEmailAddress, supervisorPassword,
        businessName, locationName, locationAddress) {
    var helper = require('sendgrid').mail;

    var from_email = new helper.Email('efeariaroo@gmail.com');

    http://ec2-54-146-182-97.compute-1.amazonaws.com/
    var to_email = new helper.Email(supervisorEmailAddress);
    var subject = 'Added as a supervisor of ' + businessName + ' on MyChange';
    var content = new helper.Content('text/html', '');

    var mail = new helper.Mail(from_email, subject, to_email, content);

    mail.personalizations[0].addSubstitution(new helper.Substitution('{{name}}', supervisorFirstName));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{password}}', supervisorPassword));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{location_name}}', locationName));
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{location_address}}', locationAddress));

    var myChangeManagerHome = Constants.MY_CHANGE_DOMAIN + "/mc-admin";
    mail.personalizations[0].addSubstitution(new helper.Substitution('{{mychange_manager_home}}', myChangeManagerHome));
    mail.setTemplateId('e0cddfa8-1adc-4555-b522-bc55ac02b381');

    var sg = require('sendgrid')(SendGridConfig.SENDGRID_API_KEY);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(request, function(error, response) {
        if(error) {
            console.log("Error in sending email to supervisor!" + JSON.stringify(error));
        } else {
            console.log(response.statusCode);
            console.log(response.body);
            console.log(response.headers);
        }
    });
};

module.exports = utils;
