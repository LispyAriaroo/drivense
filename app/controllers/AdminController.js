'use strict';


var express = require('express');

var Constants = require('../../Constants');
var Utils = require('../Utils');
var Rest = require('../enums/Rest');

var Tables = require('../enums/Tables');
var TableColumns = require('../enums/TableColumns');

var dbObject = require('../dbAccess/_DbObject');
var User = require('../dbAccess/User');
var Admin = require('../dbAccess/Admin');
var Application = require('../dbAccess/Application');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');
var momentTimezone = require('moment-timezone');

//var uuid = require('node-uuid');
var uuidV1 = require('uuid/v1');

var TokenVerifier = require('../TokenVerifier');

var adminsController = {
    setupHandlers : function (app) {
        var router = express.Router();

        app.post('/api/v1/adminlogin', handleAdminLogin);

        router.route('/allapplications')
            .post(TokenVerifier.verifyToken, handleGetAllApplications);

        router.route('/application')
            .post(TokenVerifier.verifyToken, handleGetApplicationData);

        router.route('/application/reject')
            .post(TokenVerifier.verifyToken, handleApplicationReject);

        router.route('/application/approve')
            .post(TokenVerifier.verifyToken, handleApplicationApprove);
        app.use('/api/v1', router);
    }
};

var UserColumns = TableColumns[Tables.USERS];


function handleAdminLogin(req, res) {
    var userEmail = req.body.adminEmail;
    var userPassword = req.body.adminPassword;

    Admin.findByEmail(userEmail, function(user){
        if(user) {
            var isPasswordCorrect = userPassword === user.password_hash;

            if(isPasswordCorrect) {
                delete user.password_hash;
                delete user[TableColumns.ID];

                var jwtConfigSecret = process.env.SECRET;
                var token = jwt.sign(user, jwtConfigSecret, {
                    expiresIn: Rest.JWT_USER_TIMEOUT
                });

                var response = {};
                response[Rest.SUCCESS] = true;
                response[Rest.TOKEN] = token;
                response[Rest.DATA] = user;
                response[Rest.MESSAGE] = "Admin login was successful!";
                return res.json(response);
            } else {
                var errorMsg = "Sorry, your email-address or password is wrong!";
                return Utils.spitOutFailure(errorMsg, res);
            }
        } else {
            var errorMsg = "Sorry, your email-address does not exist!";
            return Utils.spitOutFailure(errorMsg, res);
        }
    });
}

function handleGetAllApplications(req, res) {
    var adminUuid = req.decoded["admin_uuid"];
    if(adminUuid && adminUuid.length > 0) {
        Admin.findBy("admin_uuid", adminUuid, function(user){
            if(user) {
                Application.findAll().then(function(allApplications){
                    var response = {};
                    response[Rest.SUCCESS] = true;
                    response[Rest.DATA] = allApplications;
                    return res.json(response);
                });
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}

function handleGetApplicationData(req, res) {
    var adminUuid = req.decoded["admin_uuid"];
    if(adminUuid && adminUuid.length > 0) {
        Admin.findBy("admin_uuid", adminUuid, function(user){
            if(user) {
                var applicationUuid = req.body.applicationUuid;
                if(applicationUuid && applicationUuid.length > 0) {
                    Application.findByUuid(applicationUuid).then(function(application){
                        if(application) {
                            var response = {};
                            response[Rest.SUCCESS] = true;
                            response[Rest.DATA] = application[0];
                            return res.json(response);
                        } else {
                            var errorMsg = "Sorry, the application you specified could not be found!";
                            return Utils.spitOutFailure(errorMsg, res);
                        }
                    });
                } else {
                    var errorMsg = "Sorry, it seems you did not specify the application to review!";
                    return Utils.spitOutFailure(errorMsg, res);
                }
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}

function handleApplicationApprove(req, res) {
    var adminUuid = req.decoded["admin_uuid"];
    if(adminUuid && adminUuid.length > 0) {
        Admin.findBy("admin_uuid", adminUuid, function(user){
            if(user) {
                var applicationUuid = req.body.applicationUuid;
                var reviewerComment = req.body.reviewerComment;

                if(applicationUuid && applicationUuid.length > 0) {
                    dbObject.tableName = Tables.APPLICATIONS;
                    dbObject.findByPromise("application_uuid", applicationUuid).then(function(application){
                        if(application) {
                            console.log("appliction:\n" + JSON.stringify(application[0]));
                            applicationRejectOrApprove(application[0], false, true, reviewerComment, res);
                        } else {
                            var errorMsg = "Sorry, the application you specified could not be found!";
                            return Utils.spitOutFailure(errorMsg, res);
                        }
                    });
                } else {
                    var errorMsg = "Sorry, it seems you did not specify the application to review!";
                    return Utils.spitOutFailure(errorMsg, res);
                }
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}

function handleApplicationReject(req, res) {
    var adminUuid = req.decoded["admin_uuid"];
    if(adminUuid && adminUuid.length > 0) {
        Admin.findBy("admin_uuid", adminUuid, function(user){
            if(user) {
                var applicationUuid = req.body.applicationUuid;
                var reviewerComment = req.body.reviewerComment;
                console.log("reviewerComment: " + reviewerComment);

                if(applicationUuid && applicationUuid.length > 0) {
                    dbObject.tableName = Tables.APPLICATIONS;
                    dbObject.findByPromise("application_uuid", applicationUuid).then(function(application){
                        if(application) {
                            console.log("appliction:\n" + JSON.stringify(application[0]));
                            applicationRejectOrApprove(application[0], true, false, reviewerComment, res);
                        } else {
                            var errorMsg = "Sorry, the application you specified could not be found!";
                            return Utils.spitOutFailure(errorMsg, res);
                        }
                    });
                } else {
                    var errorMsg = "Sorry, it seems you did not specify the application to review!";
                    return Utils.spitOutFailure(errorMsg, res);
                }
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}


function applicationRejectOrApprove(application, reject, approve, reviewerComment, res) {
    var updatedObj = {};
    updatedObj['reviewer_comment'] = reviewerComment;
    updatedObj['is_processor_pending'] = true;

    if(reject === true) {
        updatedObj['is_reviewer_pending'] = false;
        updatedObj['did_reviewer_reject'] = true;
        updatedObj['did_reviewer_approve'] = false;

        dbObject.tableName = Tables.APPLICATIONS;
        dbObject.updateWithObject(application.id, updatedObj).then(function(){
            var response = {};
            response[Rest.SUCCESS] = true;
            response[Rest.MESSAGE] = "Application was reviewed successfully.";
            return res.json(response);
        });
    } else if(approve === true) {
        updatedObj['is_reviewer_pending'] = false;
        updatedObj['did_reviewer_reject'] = false;
        updatedObj['did_reviewer_approve'] = true;

        dbObject.tableName = Tables.APPLICATIONS;
        dbObject.updateWithObject(application.id, updatedObj).then(function(){
            var response = {};
            response[Rest.SUCCESS] = true;
            response[Rest.MESSAGE] = "Application was reviewed successfully.";
            return res.json(response);
        });
    } else {
        var errorMsg = "Sorry, that action is not supported!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}

module.exports = adminsController;
