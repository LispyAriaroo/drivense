'use strict';


var express = require('express');

var Constants = require('../../Constants');
var Utils = require('../Utils');
var Rest = require('../enums/Rest');

var nodemailer = require('nodemailer');

var Tables = require('../enums/Tables');
var TableColumns = require('../enums/TableColumns');

var dbObject = require('../dbAccess/_DbObject');
var User = require('../dbAccess/User');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');
var momentTimezone = require('moment-timezone');

//var uuid = require('node-uuid');
var uuidV1 = require('uuid/v1');

var TokenVerifier = require('../TokenVerifier');

var usersController = {
    setupHandlers : function (app) {
        var router = express.Router();

        app.post('/api/v1/applicantsignup', handleUserSignup);

        router.route('/newapplication')
            .post(TokenVerifier.verifyToken, handleNewApplication);

        router.route('/user')
            .post(TokenVerifier.verifyToken, handleGetUserBasicInfo);

        app.use('/api/v1', router);
    }
};

var UserColumns = TableColumns[Tables.USERS];


function handleUserSignup(req, res) {
    var userFirstName = req.body.userFirstName;
    var userLastName = req.body.userLastName;

    var userEmail = req.body.userEmail;
    var userPhoneNumber = req.body.userPhoneNumber;
    var userPassword = req.body.userPassword;

    var gender = req.body.gender;
    var userDateOfBirth = req.body.dateOfBirth;
    var stateOfOrigin = req.body.stateOfOrigin;

    var occupation = req.body.occupation;
    var residentialAddress = req.body.residentialAddress;

    var imageUrl = req.body.imageUrl;


    checkUserSignupInputs(userFirstName, userLastName, userEmail, userPhoneNumber, userPassword,
        onInputsOk, onInputsNotOk);

    function onInputsOk() {
        User.findByEmail(userEmail, function(doesUserEmailExist) {
            if(doesUserEmailExist) {
                var errorMsg = 'Sorry, that email belongs to another applicant!';
                return Utils.spitOutFailure(errorMsg, res);
            } else {
                userRegistration(userFirstName, userLastName, userEmail, userPhoneNumber, userPassword, res);
            }
        });
    }

    function onInputsNotOk(errorMsg) {
        return Utils.spitOutFailure(errorMsg, res);
    }
}


function checkUserSignupInputs(userFirstName, userLastName, userEmail, userPhoneNumber, userPassword,
    inputsOkCallback, inputsNotOkCallback) {
    var errorState = null;

    if(!userFirstName || userFirstName.length == 0) {
        errorState = "Please enter your first name";
    } else if(!userLastName || userLastName.length == 0) {
        errorState = "Please enter your last name";
    } else if(!userEmail || userEmail.length == 0) {
        errorState = "Please enter your email address";
    } else if(!userPhoneNumber || userPhoneNumber.length == 0) {
        errorState = "Please enter your phone number";
    } else if(!userPassword || userPassword.length == 0) {
        errorState = "Please enter your password";
    }
    if(errorState)
        inputsNotOkCallback(errorState);
    else {
        inputsOkCallback();
    }
}

function userRegistration(firstName, lastName, userEmail, userPhoneNumber, newUserPassword, res) {
    var nowDateObj = new Date();
    var nowTimeSinceEpoch = nowDateObj.getTime();
    var currentDateTime = momentTimezone(nowTimeSinceEpoch).tz('Africa/Lagos').format('YYYY-MM-DD HH:mm');

    bcrypt.hash(newUserPassword, null, null, function(err, newPasswordHash) {
        //var newUserUuid = uuid.v1();
        var newUserUuid = uuidV1();

        User.addNewUser(firstName, lastName, userEmail, userPhoneNumber, newPasswordHash, newUserUuid, currentDateTime)
        .then(function(idOfNewCustomer) {
            var response = {};
            response[Rest.SUCCESS] = true;

            var jwtConfigSecret = process.env.SECRET;
            var token = jwt.sign({
                'user_uuid' : newUserUuid
            }, jwtConfigSecret, {
                expiresIn: Rest.JWT_USER_TIMEOUT
            });

            var successMsg = "Your account was created successfully. ";
            response[Rest.MESSAGE] =  successMsg;
            response[Rest.TOKEN] = token;

            res.json(response);
        });
    });
}


function handleNewApplication(req, res) {
    var userUUID = req.decoded[UserColumns.USER_UUID];
    if(userUUID && userUUID.length > 0) {
        User.findBy(UserColumns.USER_UUID, userUUID, function(user){
            if(user) {
                var applicationType = req.body.applicationType;
                var driverTestScore = req.body.driverTestScore;
                var state = req.body.state;
                var isFirstTime = req.body.isFirstTime;

                var newApplicationUuid = uuidV1();

                var insertObj = {};
                insertObj['applicant_user_id'] = user[0].id;
                insertObj['application_uuid'] = newApplicationUuid;
                insertObj['application_type'] = applicationType;
                insertObj['driver_test_score'] = parseInt(driverTestScore);
                insertObj['state_made_from'] = state;
                insertObj['first_time_or_renewal'] = isFirstTime;

                insertObj['is_reviewer_pending'] = false;
                insertObj['is_processor_pending'] = false;

                dbObject.tableName = Tables.APPLICATIONS;
                dbObject.insertUsingPromise(insertObj).then(function(idOfNewRow){
                    //sendNewApplicationToReviewer();

                    var response = {};
                    response[Rest.SUCCESS] = true;
                    response[Rest.MESSAGE] = "Application was submitted successfully.";
                    return res.json(response);
                });
            } else {
                var errorMsg = "Sorry, your user record does NOT exist!";
                return Utils.spitOutFailure(errorMsg, res);
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }

}

function handleGetUserBasicInfo(req, res) {
    var userUUID = req.decoded[TableColumns.UUID];
    if(userUUID && userUUID.length > 0) {
        User.findBy(TableColumns.UUID, userUUID, function(user){
            if(user) {
                delete user[0].password_hash;
                delete user[0][TableColumns.ID];
                var response = {};
                response[Rest.SUCCESS] = true;
                response[Rest.DATA] = user[0];

                return res.json(response);
            } else {
                var errorMsg = "Sorry, your user record does NOT exist!";
                return Utils.spitOutFailure(errorMsg, res);
            }
        });
    } else {
        var errorMsg = "Sorry, it seems you are not a valid user on our systems!";
        return Utils.spitOutFailure(errorMsg, res);
    }
}


function sendNewApplicationToReviewer() {
    // dbObject.tableName = Tables.ADMINS;
    // dbObject.findAll(callbackOnResult) {
    //
    // }

    console.log("sendNewApplicationToReviewer!");
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'efeariaroo1@gmail.com',
            pass: 'asdffdsaOKN;925............'
        }
    });

    var emailText = "A new application has been submitted.";

    var mailOptions = {
        from: 'efeariaroo1@gmail.com',
        to: "efeariaroo@gmail.com",
        subject: 'New Application',
        text: '',
        html: emailText
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log("sendNewApplicationToReviewer! \n" + JSON.stringify(error) + "\n");
        } else {
            console.log('Inside sendNewApplicationToReviewer! message sent: ' + info + "\n");
        }
    });
}


module.exports = usersController;
