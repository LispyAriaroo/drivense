'use strict';


var Tables = {
    USERS : 'users',
    ADMINS : 'admins',
    APPLICATIONS : 'applications'
};

module.exports = Tables;
