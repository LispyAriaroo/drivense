'use strict';


var Tables = require('./Tables');

var TableColumns = {};


TableColumns['ID'] = 'id';
TableColumns['IS_ENABLED'] = 'is_enabled';
TableColumns['CREATED_AT'] = 'created_at';
TableColumns['UPDATED_AT'] = 'updated_at';

TableColumns[Tables.USERS] = {
    FIRST_NAME : 'first_name',
    LAST_NAME : 'last_name',
    EMAIL_ADDRESS : 'email_address',
    PHONE_NUMBER : 'phone_number',
    USER_UUID : 'user_uuid',

    PASSWORD_HASH : 'password_hash'
};

TableColumns[Tables.ADMINS] = {
    ADMIN_UUID : 'admin_uuid',
    EMAIL_ADDRESS : 'email_address',
    PASSWORD_HASH : 'password_hash'
};

TableColumns[Tables.APPLICATIONS] = {
    APPLICATION_UUID : 'application_uuid',
    EMAIL_ADDRESS : 'email_address',
    PASSWORD_HASH : 'password_hash'
};

module.exports = TableColumns;
