'use strict';


var Rest = {
    SUCCESS : 'success',
    MESSAGE : 'message',
    DATA : 'data',

    TOKEN : 'token',
    JWT_USER_TIMEOUT : 864000,      // 10 days
};

module.exports = Rest;
