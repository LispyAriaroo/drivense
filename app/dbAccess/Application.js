
var dbObject = require('./_DbObject');

var Tables = require('../enums/Tables');
var TableColumns = require('../enums/TableColumns');


function Application(tableName) {
    this.tableName = tableName;
}

Application.prototype = dbObject;
var applicationsTable = new Application('applications');

applicationsTable.findAll = function() {
    var whereClause = {};
    //whereClause[TableColumns[Tables.COMPANY_ADMINS].EMAIL_ADDRESS] = adminEmail;

    return this.knex(this.tableName).where(whereClause)
    .innerJoin(Tables.USERS,
        Tables.USERS + '.' + TableColumns.ID,
        Tables.APPLICATIONS + '.applicant_user_id')
    .select(
        'first_name',
        'last_name',
        'application_uuid',
        'application_type',
        'driver_test_score',
        'state_made_from',
        'first_time_or_renewal',
        'is_reviewer_pending'
    );
}

applicationsTable.findByUuid = function(applicationUuid) {
    var whereClause = {};
    whereClause["application_uuid"] = applicationUuid;

    return this.knex(this.tableName).where(whereClause)
    .innerJoin(Tables.USERS,
        Tables.USERS + '.' + TableColumns.ID,
        Tables.APPLICATIONS + '.applicant_user_id')
    .select(
        'first_name',
        'last_name',
        'application_uuid',
        'application_type',
        'driver_test_score',
        'state_made_from',
        'first_time_or_renewal'
    );
}

module.exports = applicationsTable;
