
var dbObject = require('./_DbObject');

var Tables = require('../enums/Tables');
var TableColumns = require('../enums/TableColumns');


function User(tableName) {
    this.tableName = tableName;
}

User.prototype = dbObject;
var usersTable = new User('users');

usersTable.findByEmail = function(userEmail, callbackOnResult) {
    var whereClause = {};
    whereClause[TableColumns[Tables.USERS].EMAIL_ADDRESS] = userEmail;

    this.knex(this.tableName).where(whereClause)
    .select('*').then(function (results) {
        if (results && results.length == 0) {
            callbackOnResult(null);
        } else {
            callbackOnResult(results[0]);
        }
    });
}

// usersTable.addNewUser = function(firstName, lastName, emailAddress, phoneNumber, passwordHash,
//     uuid, currentDateTime, onAdditionDone) {
//
//     var insertObj = {};
//     insertObj[TableColumns[Tables.USERS].FIRST_NAME] = firstName;
//     insertObj[TableColumns[Tables.USERS].LAST_NAME] = lastName;
//     insertObj[TableColumns[Tables.USERS].EMAIL_ADDRESS] = emailAddress;
//     insertObj[TableColumns[Tables.USERS].PHONE_NUMBER] = phoneNumber;
//     insertObj[TableColumns[Tables.USERS].PASSWORD_HASH] = passwordHash;
//     insertObj[TableColumns[Tables.USERS].USER_UUID] = uuid;
//
//     insertObj[TableColumns.CREATED_AT] = currentDateTime;
//
//     this.knex(this.tableName).insert(insertObj)
//     .returning('id').into(this.tableName)
//     .then(function (id) {
//         onAdditionDone(id);
//     });
// }


usersTable.addNewUser = function(firstName, lastName, emailAddress, phoneNumber,
        passwordHash, uuid, currentDateTime) {
    var insertObj = {};
    insertObj[TableColumns[Tables.USERS].FIRST_NAME] = firstName;
    insertObj[TableColumns[Tables.USERS].LAST_NAME] = lastName;
    insertObj[TableColumns[Tables.USERS].EMAIL_ADDRESS] = emailAddress;
    insertObj[TableColumns[Tables.USERS].PHONE_NUMBER] = phoneNumber;
    insertObj[TableColumns[Tables.USERS].PASSWORD_HASH] = passwordHash;
    insertObj[TableColumns[Tables.USERS].USER_UUID] = uuid;

    insertObj[TableColumns.CREATED_AT] = currentDateTime;

    return this.knex
    //.transacting(transaction)
    .insert(insertObj)
    .returning('id')
    .into(this.tableName);
}

usersTable.setNewPassword = function(userId, newPasswordHash, currentDateTime, onSetDone) {
    this.knex(this.tableName).where({
        'id' : userId
    })
    .update({
        'password_hash' : newPasswordHash,
        'updated_at' : currentDateTime
    })
    .then(function (count) {
        onSetDone();
    });
};

module.exports = usersTable;
