
var dbObject = require('./_DbObject');

var Tables = require('../enums/Tables');
var TableColumns = require('../enums/TableColumns');


function Admin(tableName) {
    this.tableName = tableName;
}

Admin.prototype = dbObject;
var adminsTable = new Admin('admins');

adminsTable.findByEmail = function(userEmail, callbackOnResult) {
    var whereClause = {};
    whereClause[TableColumns[Tables.USERS].EMAIL_ADDRESS] = userEmail;

    this.knex(this.tableName).where(whereClause)
    .select('*').then(function (results) {
        if (results && results.length == 0) {
            callbackOnResult(null);
        } else {
            callbackOnResult(results[0]);
        }
    });
}

module.exports = adminsTable;
