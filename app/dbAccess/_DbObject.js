var dbAccess = require('../../config/_dbConfig');

var DbObject = {};

DbObject.findAll = function(callbackOnResult) {
    var sqlSelectQuery = 'select * from ' + this.tableName;
    dbAccess.raw(sqlSelectQuery, []).then(function(results) {
        callbackOnResult(results.rows);
    });
}

DbObject.findBy = function(columnName, columnVal, callbackOnResult) {
    var queryObj = {};
    queryObj[columnName] = columnVal;

    dbAccess(this.tableName).where(queryObj).select('*').then(function (results) {
        callbackOnResult(results);
    });
}

DbObject.findByPromise = function(columnName, columnVal) {
    var queryObj = {};
    queryObj[columnName] = columnVal;

    return dbAccess(this.tableName)
    .where(queryObj)
    .select('*');
}

DbObject.insert = function(insertData, callbackOnInsertDone) {
    dbAccess(this.tableName).insert(insertData).then(function() {
        callbackOnInsertDone();
    });
}

DbObject.insertUsingPromise = function(insertData) {
    return dbAccess(this.tableName)
    .insert(insertData);
}

DbObject.updateWithObject = function(userId, updateObj) {
    return dbAccess(this.tableName)
    .where({
        'id' : userId
    })
    .update(updateObj);
};

DbObject.deleteBy = function(columnName, columnVal, callbackOnDeleteDone) {
    dbAccess(this.tableName).where(columnName, columnVal)
    .del()
    .then(function(theReturn) {
        callbackOnDeleteDone(theReturn);
    });
}

DbObject.knex = dbAccess;

module.exports = DbObject;
