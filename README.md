### Server Boilerplate


### Stuff you have to change
- .env
  Remove 'Sbm_DbEnv'

- knexfile.js
  change db parameters

  


### To start the server
The postgres database should already be running. It should have a database with name 'mychangedb'.

In development run:
```terminal
npm run startdev
```

In production run:
Use the below initially.
```terminal
pm2 start MyChangeServer.js
```

Subsequently, use this:
```terminal
pm2 start idOfMyChangeServerPm2Process
```

### Environment variables that need to be set
```
SECRET

CLOUDINARY_APP_NAME
CLOUDINARY_API_KEY
CLOUDINARY_API_SECRET

SENDGRID_API_KEY
```

##### Database schema migration
If your knexfile.js does not exist then run:
```terminal
knex init
```

```terminal
knex migrate:make newtablename
```

```terminal
knex migrate:latest --env development
```

##### Postman Collection
```
https://www.getpostman.com/collections/28d734849d598204160a
```

##### On using the Helmet library for security
```
http://scottksmith.com/blog/2014/09/21/protect-your-node-apps-noggin-with-helmet/
```

##### Libraries security check
```terminal
npm install nsp -g

nsp check
```

##### SendGrid For Sending Emails
- https://www.npmjs.com/package/sendgrid
- https://github.com/sendgrid/sendgrid-nodejs/blob/master/USE_CASES.md


##### Gulp and Combining front-end AngularJs files into one file Tutorial
Source
- https://medium.com/@dickeyxxx/best-practices-for-building-angular-js-apps-266c1a4a6917#.g0ryxx1cm

First, on the client-side folder create a package.json file with only "{}" content then run:
```
npm install --global gulp
npm install --save-dev gulp gulp-concat
```

Sample project ported from: dickeyxxx/ng-modules
```
https://github.com/LispyAriaro/ng-modules
```

Now, run when you modify any of the AngularJs scripts:
```
gulp js
```





##### References
- [NodeJs Express Security](https://expressjs.com/en/advanced/best-practice-security.html)
- [Timezone Foo](http://stackoverflow.com/questions/15347589/moment-js-format-date-in-a-specific-timezone)

For understanding database migration things with knex:
- [Knex Database migration tutorial](http://perkframework.com/v1/guides/database-migrations-knex.html)
- [Knex Database setup tutorial](http://www.dancorman.com/knex-your-sql-best-friend/)
- [Knex database migration tutorial](http://alexzywiak.github.io/running-migrations-with-knex/)
- [](https://medium.com/@HalahSalih/project-settings-for-an-express-app-with-knex-16959517b53b#.s60cgzlb8)
