
//var ServerBaseUrl = "https://www.serverdomain.com";
var ServerBaseUrl = "http://localhost:3100/";

var SHOW_LOG = true;


/* Local storage keys */

var USER_TOKEN = "userToken";

var logger = console.log;
console.log = function (logStatement) {
    if (SHOW_LOG)
        logger(logStatement);
}
