'use strict';

/*
	Doing things this way so that Utils factory can be accessed from any controller in any Angular app
	http://stackoverflow.com/questions/19841956/can-angularjs-reuse-a-service-across-several-ng-apps
*/

angular.module('UtilsModule', []).factory('Utils', function() {
	var utils = {};

	var emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(-)*(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

	utils.INPUTTYPE_TEXT = 'text';
	utils.INPUTTYPE_EMAIL = 'email';
	utils.INPUTTYPE_PHONE = 'phone';
	utils.INPUTTYPE_DATE = 'date'; // Assumes YYYY-mm-dd

	//utils.INPUT_TEXT_MAX_LENGTH = 'textmaxlength';

	utils.capitalize = function(word) {
		return word.replace( /(^|\s)([a-z])/g , function(m, p1, p2){
			return p1 + p2.toUpperCase();
		});
	};

    utils.objectToParams = function (obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + encodeURIComponent(obj[key]));
        }
        return p.join('&');
    }

    utils.isEmpty = function (obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }

	utils.sliceObj = function(theObj, beginIndex, endIndex){
		var objKeys = Object.keys(theObj);
		var slicedKeys = objKeys.slice(beginIndex, endIndex);

		var objToReturn = {};
		for (var i = 0; i < slicedKeys.length; i++) {
			var aKey = slicedKeys[i];
			objToReturn[aKey] = theObj[aKey];
		}
		return objToReturn;
	};

	utils.areAllInputsOks = function(inputsArray) {
		console.log("[RevovaUtils] Inside areAllInputsOks");
		var okState = true;

		for(var i = 0; i < inputsArray.length; ++i) {
			var input = inputsArray[i];
			var inputVal = input.value;
			var inputType = input.type || this.INPUTTYPE_TEXT;
			var doesErrorExist = false;

			if(inputType === this.INPUTTYPE_TEXT) {
				if (inputVal === '') {
					doesErrorExist = true;
				} else if(input.textmaxlength > 0) {
					var maxLength = input.textmaxlength;
					doesErrorExist = inputVal.length > maxLength;
				}
			} else if(inputType === this.INPUTTYPE_DATE) {
				doesErrorExist = !this.isValidDate(inputVal);
			} else if(inputType === this.INPUTTYPE_EMAIL) {
				doesErrorExist = !this.isEmailOk(inputVal);
			} else if(inputType === this.INPUTTYPE_PHONE) {
				doesErrorExist = inputVal.length !== 11;
			}
			if(doesErrorExist === true) {
				var inputErrorMsg = input.errorMsg || "Please enter a valid value for your " + input.name;
				okState = inputErrorMsg; // We need this for display to the user
				break;
			}
		}
		return okState;
	};

	utils.isEmailOk = function(inputVal) {
		return emailPattern.test(inputVal);
	};

	utils.isNumeric = function(input){
	    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
	    return (RE.test(input));
	}

    // Validates that the input string is a valid date formatted as "yyyy-mm-dd"
    // Reference: http://stackoverflow.com/questions/6177975/how-to-validate-date-with-format-mm-dd-yyyy-in-javascript
    utils.isValidDate = function (dateString) {
        var isOk = false;

        // First check for the pattern
        if(!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString)) {
            isOk = false;
        }

        // Parse the date parts to integers
        var parts = dateString.split("-");
        var year = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var day = parseInt(parts[2], 10);

		if(isNaN(year)) {
			isOk = false;
			return false;
		}

        // Check the ranges of month and year
        if(year < 1000 || year > 3000 || month == 0 || month > 12) {
            isOk = false;
        }

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        isOk = day > 0 && day <= monthLength[month - 1];

        if(!isOk) {
            return false;
        } else {
            return true;
        }
    };
    return utils;
});
