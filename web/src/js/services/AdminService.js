"use strict";

angular.module('app')
.factory("AdminService", ["$http", "$rootScope", function($http, $rootScope) {
    return {
        login: function(email, password) {
            return $http.post("/api/v1/adminlogin", {
                adminEmail : email,
                adminPassword : password
            });
        },
        getAllApplications: function() {
            var token = localStorage.getItem(USER_TOKEN);

            return $http.post("/api/v1/allapplications", {
                token : token
            });
        },
        getApplicationData: function(applicationUuid) {
            var token = localStorage.getItem(USER_TOKEN);

            return $http.post("/api/v1/application", {
                token : token,
                applicationUuid : applicationUuid
            });
        },
        rejectApplication: function(applicationUuid, reviewerComment) {
            var token = localStorage.getItem(USER_TOKEN);

            return $http.post("/api/v1/application/reject", {
                token : token,
                applicationUuid : applicationUuid,
                reviewerComment : reviewerComment
            });
        },
        approveApplication: function(applicationUuid, reviewerComment) {
            var token = localStorage.getItem(USER_TOKEN);

            return $http.post("/api/v1/application/approve", {
                token : token,
                applicationUuid : applicationUuid,
                reviewerComment : reviewerComment
            });
        }
    };
}]);
