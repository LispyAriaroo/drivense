"use strict";

angular.module('app')
.factory("CustomerService", ["$http", "$rootScope", function($http, $rootScope) {
    return {
        // login: function(email, userPassword) {
        //     return $http.post("/api/v1/userlogin", {
        //         userEmail : email,
        //         userPassword : userPassword
        //     });
        // },
        register: function(userFirstName, userLastName, userEmail, userPhoneNumber, userPassword,
            userGender, userDateOfBirth, userStateOfOrigin,
            userOccupation, userResidentialAddress, userImageUrl) {
            return $http.post("/api/v1/applicantsignup", {
                userFirstName : userFirstName,
                userLastName : userLastName,

                userEmail : userEmail,
                userPhoneNumber : userPhoneNumber,
                userPassword : userPassword,

                gender : userGender,
                userDateOfBirth : userDateOfBirth,
                stateOfOrigin : userStateOfOrigin,

                occupation : userOccupation,
                residentialAddress : userResidentialAddress,
                imageUrl : userImageUrl
            });
        },
        sendApplication : function(applicationType, driverTestScore, state, isFirstTime) {
            var token = localStorage.getItem(USER_TOKEN);

            return $http.post("/api/v1/newapplication", {
                token : token,
                applicationType : applicationType,
                driverTestScore : driverTestScore,
                state : state,
                isFirstTime : isFirstTime
            });
        }
    };
}]);
