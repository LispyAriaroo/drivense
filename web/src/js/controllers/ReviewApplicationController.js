
angular.module('app')
.controller('ReviewApplicationController', ['$scope', '$state', '$stateParams', '$location',
    'AdminService', 'Utils', function($scope, $state, $stateParams, $location, AdminService, Utils) {
    console.log("Inside ReviewApplicationController");

    $scope.isGettingData = false;

    $scope.isErrorPresent = false;
    $scope.error = '';

    $scope.applicationData = [];
    $scope.reviewerComment = '';


    $scope.initialize = function() {
        $scope.isGettingData = true;

        $scope.applicationUuid = $stateParams.applicationUuid;
        console.log("Application uuid: " + $scope.applicationUuid);

        AdminService.getApplicationData($scope.applicationUuid).then(function(res) {
            $scope.isGettingData = false;
            console.log("application response: " + JSON.stringify(res.data));

            if(res.data && res.data.success && res.data.success === true) {
                $scope.applicationData = res.data.data;
            } else {
                $scope.isErrorPresent = true;
                $scope.error = res.data.message;
            }
        }).catch(function(result) {
            $scope.isGettingData = false;
            alert("Error!!!!");
        });
    };

    $scope.rejectApplication = function() {
        if($scope.reviewerComment && $scope.reviewerComment.length > 0) {
            AdminService.rejectApplication($scope.applicationUuid, $scope.reviewerComment).then(function(res) {
                processReviewResponse(res);
            }).catch(function(result) {
                alert("A server error occurred.");
            });
        } else {
            $scope.isErrorPresent = true;
            $scope.error = "Please type in your comment about this application";
            setTimeout(function(){
                $scope.$apply(function(){
                    $("#errorLabel").effect("shake");
                });
            }, 500);
        }
    };

    $scope.approveApplication = function() {
        if($scope.reviewerComment && $scope.reviewerComment.length > 0) {
            AdminService.approveApplication($scope.applicationUuid, $scope.reviewerComment).then(function(res) {
                processReviewResponse(res);
            }).catch(function(result) {
                alert("A server error occurred.");
            });
        } else {
            $scope.isErrorPresent = true;
            $scope.error = "Please type in your comment about this application";
            setTimeout(function(){
                $scope.$apply(function(){
                    $("#errorLabel").effect("shake");
                });
            }, 500);
        }
    };


    function processReviewResponse(res) {
        var serverResponse = res.data;
        //console.log("serverResponse: " + JSON.stringify(serverResponse));
        if(serverResponse && serverResponse.success) {
            //$state.go('mc-admin.businesshome');
            alert(serverResponse.message);
        } else {
            $scope.isErrorPresent = true;
            $scope.error = serverResponse.message;
        }
    }
}]);
