
angular.module('app')
.controller('HomeController', ['$scope', '$state', '$location',
    'CustomerService', 'Utils', function($scope, $state, $location, CustomerService, Utils) {
    console.log("Inside HomeController");

    $scope.initialize = function() {
        console.log("Inside HomeController initiailize");
        if(localStorage.getItem(USER_TOKEN)){
            //$state.go('home.applicantHome');            
            var path = $location.path();
            $location.url(path);
        } else {
            $state.go('home.register');
        }
    };
}]);
