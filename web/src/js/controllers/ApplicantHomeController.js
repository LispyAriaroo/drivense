
angular.module('app')
.controller('ApplicantHomeController', ['$scope', '$state', '$location',
    'CustomerService', 'Utils', function($scope, $state, $location, CustomerService, Utils) {
    console.log("Inside ApplicantHomeController");

    $scope.isErrorPresent = false;
    $scope.error = '';
    //--
    $scope.applicationType = '';
    $scope.driverTestScore = '';
    $scope.state = '';
    $scope.isFirstTime = '';


    $scope.initialize = function() {
        console.log("Inside ApplicantHomeController initiailize");

        $(".dropdown-menu li a").click(function(){
            $("#applicationTypeButton").text($(this).text());
            $("#applicationTypeButton").val($(this).text());
        });
        //$state.go('home.register');
    };


    $scope.validateThenSend = function() {
        var inputState = validateApplicationInputs();
        if(inputState == true) {
            $scope.isErrorPresent = false;
            $scope.error = '';

            CustomerService.sendApplication($scope.applicationType, $scope.driverTestScore, $scope.state, $scope.isFirstTime)
            .then(function(res) {
                processApplicationResponse(res);
            }).catch(function(result) {
                alert("A server error occurred in sending your application.");
            });
        } else {
            $scope.isErrorPresent = true;
            $scope.error = inputState;
            setTimeout(function(){
                $scope.$apply(function(){
                    $("#signupErrorLabel").effect("shake");
                });
            }, 500);
        }
    }


    function processApplicationResponse(res) {
        var serverResponse = res.data;
        console.log("serverResponse: " + JSON.stringify(serverResponse));
        if(serverResponse && serverResponse.success) {
            alert(serverResponse.message);
        } else {
            $scope.isErrorPresent = true;
            $scope.error = serverResponse.message;
        }
    }

    function validateApplicationInputs() {
        $scope.applicationType = $('#applicationTypeButton').val();
        $scope.isFirstTime = $('input[name=isFirstTime]:checked').val();

        if($scope.isFirstTime) {
            var inputsAreOk = Utils.areAllInputsOks([
                {name : 'application type', value : $scope.applicationType, type : Utils.INPUTTYPE_TEXT,
                    errorMsg: 'Please enter application type.'},
                {name : 'driver test score', value : $scope.driverTestScore, type : Utils.INPUTTYPE_TEXT,
                    errorMsg: 'Please enter driver test score.'},
                {name : 'email', value : $scope.state, type : Utils.INPUTTYPE_TEXT,
                    errorMsg: 'Please enter state.'},
            ]);
            if(inputsAreOk === true) {
                if($scope.driverTestScore >= 0 && $scope.driverTestScore <= 100){
                    return true;
                } else {
                    return "Drive test score should be between 0 and 100"
                }
            } else {
                return inputsAreOk;
            }
        } else {
            return "Please choose if this is your first time";
        }
    }

}]);
