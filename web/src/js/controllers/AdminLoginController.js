
angular.module('app')
.controller('AdminLoginController', ['$scope', '$state', '$location',
    'AdminService', 'Utils', function($scope, $state, $location, AdminService, Utils) {
    console.log("Inside AdminLoginController.");

    $scope.isLoginErrorPresent = false;
    $scope.loginError = '';
    //$scope.isLoginData = false;
    //--
    $scope.adminEmail = '';
    $scope.adminPassword = '';

    $scope.initialize = function() {
        console.log("Inside AdminLoginController initiailize");


    };

    $scope.validateThenLogIn = function() {
        var inputState = validateLoginInputs();
        if(inputState == true) {
            $scope.isLoginErrorPresent = false;
            $scope.loginError = '';

            AdminService.login($scope.adminEmail, $scope.adminPassword).then(function(res) {
                //console.log("Business login response: " + JSON.stringify(res.data));
                //$scope.isSendingBusinessLoginData = false;
                processLoginResponse(res);
            }).catch(function(result) {
                alert("A log-in error occurred.");
            });
        } else {
            $scope.isLoginErrorPresent = true;
            $scope.loginError = inputState;
            setTimeout(function(){
                $scope.$apply(function(){
                    $("#loginErrorLabel").effect("shake");
                });
            }, 500);
        }
    }


    function processLoginResponse(res) {
        var serverResponse = res.data;
        console.log("serverResponse: " + JSON.stringify(serverResponse));
        if(serverResponse && serverResponse.success) {
            //$state.go('mc-admin.businesshome');
            alert(serverResponse.message);
            localStorage.setItem(USER_TOKEN, serverResponse.token);

            if(serverResponse.data.is_reviewer) {
                $state.go('home.reviewerHome');
            } else {

            }
        } else {
            $scope.isLoginErrorPresent = true;
            $scope.loginError = serverResponse.message;
        }
    }

    function validateLoginInputs() {
        return Utils.areAllInputsOks([
            {name : 'email', value : $scope.adminEmail, type : Utils.INPUTTYPE_EMAIL,
                errorMsg: 'Please enter a valid email address.'},
            {name : 'password', value : $scope.adminPassword, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your password.'}
        ]);
    }
}]);
