
angular.module('app')
.controller('ReviewerAllApplicationsController', ['$scope', '$state', '$location',
    'AdminService', 'Utils', function($scope, $state, $location, AdminService, Utils) {
    console.log("Inside ReviewerAllApplicationsController");

    $scope.isGettingData = false;

    $scope.isErrorPresent = false;
    $scope.error = '';

    $scope.allApplications = [];


    $scope.initializeAllApplicationsTab = function() {
        $scope.getAllApplications();
    };

    $scope.getAllApplications = function() {
        console.log("Inside getAllApplications");
        $scope.isGettingData = true;

        AdminService.getAllApplications().then(function(res) {
            $scope.isGettingData = false;
            console.log("all applications response: " + JSON.stringify(res.data));

            if(res.data && res.data.success && res.data.success === true) {
                $scope.allApplications = res.data.data;
            } else {
                $scope.isErrorPresent = true;
                $scope.error = res.data.message;
            }
        }).catch(function(result) {
            $scope.isGettingData = false;
        });
    };
}]);
