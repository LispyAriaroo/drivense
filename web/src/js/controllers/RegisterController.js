
angular.module('app')
.controller('RegisterController', ['$scope', '$state', '$location',
    'CustomerService', 'Utils', 'Facebook', function($scope, $state, $location, CustomerService, Utils, Facebook) {
    console.log("Inside RegisterController.");

    $scope.isSignupErrorPresent = false;
    $scope.signupError = '';
    //--
    $scope.firstName = '';
    $scope.lastName = '';
    $scope.userEmail = '';
    $scope.userPhoneNumber = '';
    $scope.userPassword = '';

    $scope.dateOfBirth = '';

    $scope.occupation = '';
    $scope.residentialAddress = '';
    $scope.imageUrl = '';


    $scope.initialize = function() {
        console.log("Inside RegisterController initiailize");

    };

    $scope.doFacebookLogin = function() {
        Facebook.getLoginStatus(function(response) {
            if(response.status === 'connected') {
                $scope.loggedIn = true;
                Facebook.api('/me?fields=email,name,id', function(response) {
                    $scope.user = response;

                    console.log("Response:\n" + JSON.stringify(response));
                    var userFullName = response.name;
                    $scope.firstName = userFullName.split(" ")[0];
                    $scope.lastName = userFullName.split(" ")[1];
                });
                console.log("Login status: connected");
            } else {
                $scope.loggedIn = false;
                Facebook.login(function(response) {
                    // Do something with response.
                    console.log("Facebook login: " + JSON.stringify(response));
                });
            }
        });
    }


    $scope.validateThenSignup = function() {
        var inputState = validateSignupInputs();
        if(inputState == true) {
            $scope.isSignupErrorPresent = false;
            $scope.signupError = '';

            //userGender, userDateOfBirth, userStateOfOrigin,

            CustomerService.register($scope.firstName, $scope.lastName, $scope.userEmail, $scope.userPhoneNumber, $scope.userPassword,
                '', '', '',
                $scope.occupation, $scope.residentialAddress, $scope.imageUrl).then(function(res) {
                processSignupResponse(res);
            }).catch(function(result) {
                alert("A signup server error occurred.");
            });
        } else {
            $scope.isSignupErrorPresent = true;
            $scope.signupError = inputState;
            setTimeout(function(){
                $scope.$apply(function(){
                    $("#signupErrorLabel").effect("shake");
                });
            }, 500);
        }
    }


    function processSignupResponse(res) {
        var serverResponse = res.data;
        //console.log("serverResponse: " + JSON.stringify(serverResponse));
        if(serverResponse && serverResponse.success) {
            //$state.go('mc-admin.businesshome');
            localStorage.setItem(USER_TOKEN, serverResponse.token);

            alert(serverResponse.message);
            $state.go('home.applicantHome');
        } else {
            $scope.isSignupErrorPresent = true;
            $scope.signupError = serverResponse.message;
        }
    }

    function validateSignupInputs() {
        return Utils.areAllInputsOks([
            {name : 'first name', value : $scope.firstName, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your first name.'},
            {name : 'last name', value : $scope.lastName, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your last name.'},

            {name : 'email', value : $scope.userEmail, type : Utils.INPUTTYPE_EMAIL,
                errorMsg: 'Please enter a valid email address.'},
            {name : 'password', value : $scope.userPassword, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your password.'},

            {name : 'occupation', value : $scope.occupation, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your occupation.'},
            {name : 'residential address', value : $scope.residentialAddress, type : Utils.INPUTTYPE_TEXT,
                errorMsg: 'Please enter your residential address.'}
        ]);
    }

}]);
