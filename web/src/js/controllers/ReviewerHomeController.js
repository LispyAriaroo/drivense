
angular.module('app')
.controller('ReviewerHomeController', ['$scope', '$state', '$location',
    'CustomerService', 'Utils', function($scope, $state, $location, CustomerService, Utils) {
    console.log("Inside ReviewerHomeController");

    $scope.initialize = function() {
        console.log("Inside ReviewerHomeController initiailize");
        if(localStorage.getItem(USER_TOKEN)){
            $state.go('home.reviewerHome.allApplications');
        } else {
            $state.go('home.adminlogin');
        }
    };
}]);
