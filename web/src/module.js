
var myApp = angular.module('app', ['ui.router', 'UtilsModule', 'uiRouterStyles', 'facebook']);

myApp.config(function($stateProvider, $urlRouterProvider, $locationProvider, FacebookProvider) {
    FacebookProvider.init('288792481536385');

    $stateProvider
        .state('home', {
            url:'/home',
            templateUrl: 'src/templates/home.html',
            controller: 'HomeController'
        }).state('home.register', {
            url:'/register',
            templateUrl: 'src/templates/register.html',
            controller: 'RegisterController'
        }).state('home.adminlogin', {
            url:'/adminLogin',
            templateUrl: 'src/templates/adminLogin.html',
            controller: 'AdminLoginController'
        }).state('home.applicantHome', {
            url:'/applicantHome',
            templateUrl: 'src/templates/applicantHome.html',
            controller: 'ApplicantHomeController'
        }).state('home.reviewerHome', {
            url:'/reviewerHome',
            templateUrl: 'src/templates/reviewerHome.html',
            controller: 'ReviewerHomeController'
        }).state('home.reviewerHome.allApplications', {
            url:'/reviewerAllApplications',
            templateUrl: 'src/templates/reviewerAllApplications.html',
            controller: 'ReviewerAllApplicationsController'
        }).state('home.reviewApplication', {
            url:'/reviewApplication/:applicationUuid',
            templateUrl: 'src/templates/reviewApplication.html',
            controller: 'ReviewApplicationController'
        });

    $locationProvider.html5Mode(true);      // requires  <base href="/"> in index.html
    $urlRouterProvider.otherwise('/home');
});

myApp.run(function($rootScope, $state) {
    $rootScope.isLoggedIn = function (sId, fCallback) {
        if(localStorage.getItem(USER_TOKEN)){
            return true;
        } else {
            return false;
        }
    };

    $rootScope.logOut = function (sId, fCallback) {
        console.log("Inside logOut of rootScope!");
        //var value = localStorage.getItem(OWNER_OR_SUPERVISOR_DATA);

        localStorage.clear();
        $state.go('home.register');
    };
})
