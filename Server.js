"use strict";

var express = require("express");
var helmet = require('helmet');

var Constants = require('./Constants');
var routes = require('./routes');

var app = express();

(function configureWebServer() {
    var bodyParser = require('body-parser');
    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

    app.set("view options", {layout: false});
    app.use(express.static(__dirname + '/web'));

    // http://stackoverflow.com/questions/4529586/render-basic-html-view-in-node-js-express
    app.set('views', __dirname + '/web');
    app.engine('html', require('ejs').renderFile);
    //app.set('view engine', 'ejs');

    require('dotenv').load();

    //--Helmet security
    app.use(helmet());

    routes.init(app);
    var server = app.listen(Constants.SERVER_PORT);

    console.log("\n[Server] is ready to kick ass. Port: " + Constants.SERVER_PORT);
})();
