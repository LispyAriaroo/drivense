'use strict';


var Constants = require('./Constants');

module.exports = {
    init : function (app) {
        app.set('superSecret', process.env.SECRET); // secret variable

        app.get('/', function(req, res) {
            res.render('index.html');
        });
        app.get('/*', function(req, res) {
            res.render('index.html');
        });


        var Controllers = [
            'UsersController',
            'AdminController'
        ];
        Controllers.map(function(controllerName) {
            var controller = require('./app/controllers/' + controllerName);
            controller.setupHandlers(app);
        });
    }
};
